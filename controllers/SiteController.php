<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Entradas;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionListar() {
       $b=Entradas::find()
                ->asArray() // crea un array de arrays asociativos
                ->all();
        
        return $this->render("listarTodos",[
           "datos"=>$b, 
        ]);
        
    }
    
    public function actionListar1() {
        $s=Entradas::find()->all();
        
        return $this->render("listarTodos",[
            "datos"=>$s,
        ]);
    }
    
    public function actionListar2() {
        $s=Entradas::find()
                ->select(["texto"])
                ->asArray()
                ->all();//un array de datos
        
        return $this->render("listarTodos",[
            "datos"=>$s,
        ]);
    }
    
    public function actionListar3() {
        $s=Entradas::find()
                ->select(["texto"])
                ->all();//un array de modelos
        
        return $this->render("listarTodos",[
            "datos"=>$s,
        ]);
    }
    
    public function actionListar4() {
        $salida=new Entradas();//un modelo basado en ActiveRecord vacio
        
        return $this->render("listarTodos",[
            "datos"=>$salida->find()->all(),
        ]);
    }
    
    public function actionListar5() {
        $salida=new Entradas();
        
        return $this->render("listarTodos",[
            "datos"=>$salida->findOne(1),
            //Mas correcto este que el de arriba: "datos"=>Entradas::findOne(1),
            //"datos"=>Entradas::find()->where("id=1")->one(),
        ]);
    }
    
    public function actionListar6() {
       
        return $this->render("listarTodos",[
            "datos"=>Yii::$app->db->createCommand("select * from entradas")->queryAll(),
        ]);
    }
    
    public function actionMostrar() {
        $dataProvider=new ActiveDataProvider([
            "query"=>Entradas::find(),
        ]);
        return $this->render('mostrar',[
            'dataProvider'=>$dataProvider
        ]);
    }
    
    public function actionMostraruno() {
        return $this->render('mostrarUno',[
            'model'=>Entradas::findOne(1)
        ]);
    }
    
    //cosecha propia, la practica no lo pide:
    public function actionConsulta1(){
        //mostrar todos los textos de la tabla entradas
        //voy a realizar la consulta utilizando directamente createCommand y SQL
        
        $salida=Yii::$app->db->createCommand("select texto from entradas");//clase comando sql
        $salida1=$salida->queryAll(); //array con todos los registros
        
        return $this->render("vistaConsulta1",[
            "registros"=>$salida1
        ]);
    }
    
    public function actionConsulta2(){
        //mostrar todos los id de la tabla entradas
        //voy a realizar la consulta utilizando directamente createCommand y SQL
        $salida=Yii::$app
                ->db
                ->createCommand("select id from entradas")
                ->queryAll();
        
        
        return $this->render("vistaConsulta2",[
            "registros"=>$salida
        ]);
        
    }
    //Mejor usar esta(activeRecords):
    public function actionConsulta3(){
        //mostrar todos los id de la tabla entradas
        //voy a realizar la consulta utilizando activeRecord
        //necesito el modelo Entradas
        $salida=Entradas::find()->select("texto");//esto es un activeQuery
        $salida1=$salida->all();//array de activeRecords
        
        return $this->render("vistaConsulta3",[
            "registros"=>$salida1,
        ]);
        
    }
    
    public function actionConsulta4(){
        //mostrar todos los id de la tabla entradas
        //voy a realizar la consulta utilizando activeRecord
        //necesito el modelo Entradas
        $salida=Entradas::find()->select("id")->all();
        
        return $this->render("vistaConsulta4",[
           "registros"=>$salida, 
        ]);
    }
    
    public function actionConsulta5(){
        //muestre todos los datos de la tabla entradas
        //utilizando createCommand
        $salida=Yii::$app
                ->db
                ->createCommand("select * from entradas")
                ->queryAll();
        
        
        return $this->render("vistaConsulta5",[
            "registros"=>$salida
        ]);
    }
    
    public function actionConsulta6(){
        //muestre todos los datos de la tabla entradas
        //utilizando activeRecord
        $salida=Entradas::find()->all();
        
        return $this->render("vistaConsulta6",[
           "registros"=>$salida, 
           "modelo"=>new Entradas(),
        ]);
    }
    
    public function actionConsulta7() {
        //muestre todos los datos de la tabla entradas
        //para mostrarlos en un GridView
        $consultaActiva=Entradas::find();//activeQuery 
        
        //creo un activeDataProvider con la ActiveQuery
        $dataProvider = new ActiveDataProvider([
            'query' => $consultaActiva,
        ]);
        
        return $this->render("vistaConsulta7",[
            'datos'=>$dataProvider,
        ]);
    }
    
    public function actionConsulta8() {
        //muestre todos los datos de la tabla entradas pero SOLO EL CAMPO TEXTO
        //para mostrarlos en un GridView
        $proveedorDatos=new ActiveDataProvider([
            'query'=>Entradas::find()->select('texto')
        ]);
        
        return $this->render("vistaConsulta8",[
            'datos'=>$proveedorDatos,
        ]);
    }
    
    public function actionConsulta9() {
        //muestre todas las entradas
        //a traves de un listview
        $proveedorDatos=new ActiveDataProvider([
            'query'=>Entradas::find(),
        ]);
        
        return $this->render("vistaConsulta9",[
            'datos'=>$proveedorDatos,
        ]);
    }
    
    public function actionConsulta10() {
        //muestre todos los datos de la tabla entradas pero SOLO EL CAMPO TEXTO
        //para mostrarlos en un ListView
        $proveedor=new ActiveDataProvider([
            'query'=>Entradas::find()->select('texto')
        ]);
        
        return $this->render("vistaConsulta10",[
            'datos'=>$proveedor,
        ]);
    }
    
    public function actionConsulta11() {
        //mostrar el texto de la noticia con id=1
        //utilizando DetailView
        
        //creo un modelo utilizando activeRecord
        $registroActivo=Entradas::find()->select("texto")->where("id=1")->one();
        
        return $this->render("vistaConsulta11",[
            "modelo"=>$registroActivo,
        ]);
    }
    
    public function actionConsulta12() {
        //mostrar todos los registros de entradas 
        //utilizando sqldataprovider y gridView
        
        $datos=new SqlDataProvider([
           'sql'=>'select * from entradas', 
        ]);
        
        return $this->render("vistaConsulta12",[
            'datos'=>$datos,
        ]);
    }
}
